import java.util.Stack;


public class challenge
{
	public static void main(String[] args)
	{	
		String twoOps = "+ 2 - 5 2";
		System.out.println("The expected value of the twoOp testcase is 5, the calculated value is:  " + evaluate(twoOps));
		String noOp = "2";
		System.out.println("The expected value of the noOp testcase is 2, the calculated value is: " + evaluate(noOp));
		String largeNum = "+ 100 2";
		System.out.println("The expected value of the largeNum testcase is 102, the calculated value is: " + evaluate(largeNum));
		String negativeAnswer = "- 1 5";
		System.out.println("The expected value of the negativeAnswer testcase is -4, the calculated value is: " + evaluate(negativeAnswer));
		String negativeOperand = "+ -5 4";
		System.out.println("The expected value of the negativeOperand testcase is -1, the calculated value is: " + evaluate(negativeOperand));
		String serialOps = "+ + 2 3 4";
		System.out.println("The expected value of the serialOps testcase is 9, the calculated value is: " + evaluate(serialOps));
	}

	public static Integer operation (int x, int y, String op)
	{
		if (op.equals("+"))
		{
			return x + y;
		}
		else
		{
			return x - y;
		}
	}

	public static int evaluate (String exp)
	{
		Stack<Integer> expStack = new Stack<>();
		String[] parseExp = exp.split("\\s+");
		
		int expLength = parseExp.length; 
		if (expLength == 1)
		{
			return Integer.parseInt(exp);
		}
		
		for (int i = expLength - 1; i >= 0; i--)
		{
			String currentString = parseExp[i];
			if (!currentString.equals("+") && !currentString.equals("-"))
			{
				expStack.push(Integer.parseInt(currentString));
			}
			else
			{
				int val1 = (int) expStack.pop();
				int val2 = (int) expStack.pop();
				expStack.push(operation(val1, val2, currentString));
			}
		}
		return expStack.pop();
	}
}
